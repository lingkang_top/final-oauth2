# final-oauth2
一个轻量级oauth2授权框架，可优雅整合于spring boot、spring cloud体系。
<br><br>
查看wiki：[---- wiki 说明 ----](https://gitee.com/lingkang_top/final-oauth2/wikis)

# 快速入门
未上传中央仓库，直接下载引入jar。
```xml
<dependency>
    <groupId>top.lingkang</groupId>
    <artifactId>final-security-core</artifactId>
    <version>2.0.0</version>
    <scope>system</scope>
    <systemPath>${project.basedir}/lib/final-oauth2-core-2.0.0.jar</systemPath>
</dependency>
```
application.propertie中
```properties
# 开启bean同名重写，以便于定制您的应用！
spring.main.allow-bean-definition-overriding=true
```

### 服务端
编写一个spring boot授权服务，ip端口为`127.0.0.1:8080`，并添加以下配置。
```java
@EnableAuthorizationServer // 开启授权服务
@Configuration
public class Oauth2ServerConfig extends AuthorizationServerConfiguration {
}
```
启动项目。

### 资源客户端
编写一个spring boot资源客户端，ip端口为`127.0.0.1:8081`，并添加以下配置。
```java
@EnableResourceServer// 开启资源服务
@Configuration
public class Oauth2ClientConfig extends ResourceServerConfiguration {
}
```
启动项目。
直接访问 `http://127.0.0.1:8081` 将被拦截.<br>
![输入图片说明](https://gitee.com/lingkang_top/final-oauth2/raw/master/doc/img/1.png) <br>
因为未带上token进行访问。我们使用apipost登录获得访问令牌。<br>
![输入图片说明](https://gitee.com/lingkang_top/final-oauth2/raw/master/doc/img/2.png) <br>
再访问：`http://127.0.0.1:8081?access_token=afacd236-46c5-40d6-97c3-989e3739d310` 即可正常访问。
<br>
<br>
实际应用中已经封装 `ResourceServerHolder` 装配后即可调用 `resourceServerHolder.login(username, password);`直接登录。

## 配置
### 配置服务端
```java
@EnableAuthorizationServer // 开启授权服务
@Configuration
public class Oauth2ServerConfig extends AuthorizationServerConfiguration {
    @Override
    protected void config(AuthorizationServerProperties config) {
        // 基本配置
    }

    @Override
    protected void configClient(ClientDetailsServiceBuilder clients) {
        // 客户端认证
    }
}
```

### 配置资源客户端
```java
@EnableResourceServer// 开启资源服务
@Configuration
public class Oauth2ClientConfig extends ResourceServerConfiguration {
    @Override
    protected void config(ResourceServerProperties properties) {
        properties.setExcludePath("/code/**", "/", "/password/**");// 配置排除路径
        properties.setServerUrl("http://127.0.0.1:8080");// 配置授权服务ip
        /*properties.setCheckAuthorize(new CheckAuthorize()
                .pathMatchers("/**")
                .hasAnyRole("user")
        );*/
    }
}
```

## 注意事项
* 授权服务端、资源客户端默认开放 `/oauth2/**`拦截，如果您有其他拦截器、或者过滤器，应该开放 `/oauth2/**` 路径，否则无法正常授权访问。
* 默认的oauth2密码模式登录参数
```yaml
grant_type:password
client_id:client_id
client_secret:secret
username:final
password:final
```