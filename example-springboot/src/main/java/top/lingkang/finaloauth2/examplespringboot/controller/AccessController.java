package top.lingkang.finaloauth2.examplespringboot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import top.lingkang.finaloauth2.client.annotation.Oauth2CheckRole;
import top.lingkang.finaloauth2.client.http.ResourceServerHolder;

/**
 * @author lingkang
 * Created by 2022/4/7
 */
@Oauth2CheckRole(anyRole = {"user"},allRole = {"admin"})
@RestController
public class AccessController {
   /* @Autowired
    private ResourceServerHolder resourceServerHolder;

    @GetMapping("/api")
    public Object api() {
        return resourceServerHolder.getAccessToken();
    }*/
}
