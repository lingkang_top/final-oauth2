package top.lingkang.finaloauth2.examplespringboot.config;

import org.springframework.context.annotation.Configuration;
import top.lingkang.finaloauth2.client.annotation.EnableOauth2Annotation;
import top.lingkang.finaloauth2.client.annotation.EnableResourceServer;
import top.lingkang.finaloauth2.client.base.CheckAuthorize;
import top.lingkang.finaloauth2.client.config.ResourceServerConfiguration;
import top.lingkang.finaloauth2.client.config.ResourceServerProperties;

/**
 * @author lingkang
 * Created by 2022/3/23
 */
@EnableResourceServer
@EnableOauth2Annotation
@Configuration
public class Oauth2ClientConfig extends ResourceServerConfiguration {
    @Override
    protected void config(ResourceServerProperties properties) {
        // 不鉴权路径
        properties.setExcludePath("/code/**", "/", "/password/**");
        // 配置服务端ip端口
        properties.setServerUrl("http://127.0.0.1:8080");

        // 配置鉴权路径
        properties.setCheckAuthorize(new CheckAuthorize()
                .pathMatchers("/**")
                .hasAnyRole("user")
        );

        // 配置客户端id
        properties.setClientId("client_id");
        properties.setClientSecret("secret");
        properties.setScope("all");

        // 自定义获取token的方式，，，默认是读取请求头的token参数、请求参数的access_token参数。
        // 详情见 DefaultResourceServerGetToken , 您也阔以扩展从cookie中获取。
        properties.setResourceServerGetToken(request -> {
            // return request.getHeader("token");// 从请求头中获取token
            return request.getParameter("access_token");// 从请求参数中获取token
        });
    }
}
