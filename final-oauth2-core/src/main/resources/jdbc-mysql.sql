/*
 Navicat Premium Data Transfer

 Source Server         : localhost-5.7.35
 Source Server Type    : MySQL
 Source Server Version : 50735
 Source Host           : localhost:3306
 Source Schema         : test

 Target Server Type    : MySQL
 Target Server Version : 50735
 File Encoding         : 65001

 Date: 01/04/2022 17:08:56
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for fo_client
-- ----------------------------
DROP TABLE IF EXISTS `fo_client`;
CREATE TABLE `fo_client`  (
  `client_id` varchar(225) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `client_secret` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `grant_type` varchar(225) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '授权类型例如：password,code,refresh_token 均为字母',
  `scope` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`client_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of fo_client
-- ----------------------------
INSERT INTO `fo_client` VALUES ('client_id', 'secret', 'password,refresh_token,code', 'all');

-- ----------------------------
-- Table structure for fo_code
-- ----------------------------
DROP TABLE IF EXISTS `fo_code`;
CREATE TABLE `fo_code`  (
  `code` varchar(125) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '授权码',
  `expiry` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '失效时间',
  `scope` varchar(125) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '作用域',
  `client_id` varchar(125) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户端id',
  `state` varchar(125) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '状态',
  `redirect_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '重定向url',
  PRIMARY KEY (`code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of fo_code
-- ----------------------------

-- ----------------------------
-- Table structure for fo_refresh_token
-- ----------------------------
DROP TABLE IF EXISTS `fo_refresh_token`;
CREATE TABLE `fo_refresh_token`  (
  `refresh_token` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `access_token` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_details` varchar(2048) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `expiry` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'long',
  PRIMARY KEY (`refresh_token`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of fo_refresh_token
-- ----------------------------
INSERT INTO `fo_refresh_token` VALUES ('653f131a-e063-424f-a987-6c7c475cf0ed', '{\"expiry\":1648713901699,\"refreshToken\":\"653f131a-e063-424f-a987-6c7c475cf0ed\",\"scope\":\"all\",\"token\":\"25ef366a-9260-4579-a725-f8dbf6357a97\"}', '{\"clientId\":\"client_id\",\"data\":{},\"role\":[],\"username\":\"final\"}', '1650008101699');
INSERT INTO `fo_refresh_token` VALUES ('bdbab9e3-c7c7-4114-9e5f-2d35d36c6a61', '{\"expiry\":1648716311115,\"refreshToken\":\"bdbab9e3-c7c7-4114-9e5f-2d35d36c6a61\",\"scope\":\"all\",\"token\":\"b46160f1-c1e3-498e-b2cf-3ab3337dc9ca\"}', '{\"clientId\":\"client_id\",\"data\":{},\"role\":[],\"username\":\"final\"}', '1650010511115');

-- ----------------------------
-- Table structure for fo_token
-- ----------------------------
DROP TABLE IF EXISTS `fo_token`;
CREATE TABLE `fo_token`  (
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `refresh_token` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `expiry` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'long',
  `scope` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`token`) USING BTREE,
  INDEX `refresh_token`(`refresh_token`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of fo_token
-- ----------------------------
INSERT INTO `fo_token` VALUES ('21a4aa88-a181-46de-bef8-834a4ffc6f2e', NULL, '1648805819339', 'all');
INSERT INTO `fo_token` VALUES ('9f5844ca-6bd1-411e-a2d8-b376aeb07b3b', 'bdbab9e3-c7c7-4114-9e5f-2d35d36c6a61', '1648803328305', 'all');

-- ----------------------------
-- Table structure for fo_user_details
-- ----------------------------
DROP TABLE IF EXISTS `fo_user_details`;
CREATE TABLE `fo_user_details`  (
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `client_id` varchar(225) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `refresh_token` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `details` varchar(2048) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`username`) USING BTREE,
  INDEX `token`(`token`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of fo_user_details
-- ----------------------------
INSERT INTO `fo_user_details` VALUES ('auth-code-client_id', 'client_id', '21a4aa88-a181-46de-bef8-834a4ffc6f2e', NULL, '{\"clientId\":\"client_id\",\"data\":{},\"role\":[],\"username\":\"auth-code-client_id\"}');
INSERT INTO `fo_user_details` VALUES ('final', 'client_id', '9f5844ca-6bd1-411e-a2d8-b376aeb07b3b', 'bdbab9e3-c7c7-4114-9e5f-2d35d36c6a61', '{\"clientId\":\"client_id\",\"data\":{},\"role\":[],\"username\":\"final\"}');

SET FOREIGN_KEY_CHECKS = 1;
