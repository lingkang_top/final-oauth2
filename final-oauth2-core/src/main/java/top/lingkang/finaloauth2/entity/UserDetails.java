package top.lingkang.finaloauth2.entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * @author lingkang
 * Created by 2022/3/25
 * 使用接口定义用户详情，可用于自定义属性扩展
 */
public class UserDetails implements Serializable{
    private String username;
    private String clientId;
    private Set<String> role=new HashSet<>();
    private HashMap<String,Object> data=new HashMap<>();

    public UserDetails() {
    }

    public UserDetails(String username, Set<String> role) {
        this.username = username;
        this.role = role;
    }

    @Override
    public String toString() {
        return "UserDetails{" +
                "username='" + username + '\'' +
                ", clientId='" + clientId + '\'' +
                ", role=" + role +
                ", data=" + data +
                '}';
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public Set<String> getRole() {
        return role;
    }

    public void setRole(Set<String> role) {
        this.role = role;
    }

    public HashMap<String, Object> getData() {
        return data;
    }

    public void setData(HashMap<String, Object> data) {
        this.data = data;
    }
}
