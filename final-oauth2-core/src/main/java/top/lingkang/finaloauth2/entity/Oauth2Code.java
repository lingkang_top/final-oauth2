package top.lingkang.finaloauth2.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @author lingkang
 * Created by 2022/3/29
 */
public class Oauth2Code implements Serializable {
    private String code;
    private long expiry;
    private Set<String> scope=new HashSet<>();
    private String redirectUri;
    private String state;
    private String clientId;

    @Override
    public String toString() {
        return "Oauth2Code{" +
                "code='" + code + '\'' +
                ", expiry=" + expiry +
                ", scope=" + scope +
                ", redirectUri='" + redirectUri + '\'' +
                ", state='" + state + '\'' +
                ", clientId='" + clientId + '\'' +
                '}';
    }

    public String getRedirectUri() {
        return redirectUri;
    }

    public void setRedirectUri(String redirectUri) {
        this.redirectUri = redirectUri;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public long getExpiry() {
        return expiry;
    }

    public void setExpiry(long expiry) {
        this.expiry = expiry;
    }

    public Set<String> getScope() {
        return scope;
    }

    public void setScope(Set<String> scope) {
        this.scope = scope;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }
}
