package top.lingkang.finaloauth2.entity;

import java.io.Serializable;

/**
 * @author lingkang
 * Created by 2022/3/30
 */
public class UserTokenInfo implements Serializable {
    private String token;
    private String refreshToken;
    private long tokenExpires;
    private long refreshExpires;

    public long getTokenExpires() {
        return tokenExpires;
    }

    public void setTokenExpires(long tokenExpires) {
        this.tokenExpires = tokenExpires;
    }

    public long getRefreshExpires() {
        return refreshExpires;
    }

    public void setRefreshExpires(long refreshExpires) {
        this.refreshExpires = refreshExpires;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
}
