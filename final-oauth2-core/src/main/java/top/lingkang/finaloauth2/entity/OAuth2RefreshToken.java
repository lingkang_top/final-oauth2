package top.lingkang.finaloauth2.entity;

import java.io.Serializable;

/**
 * @author lingkang
 * Created by 2022/3/24
 */
public class OAuth2RefreshToken implements Serializable {
    private String refreshToken;
    private long expiry;
    private OAuth2AccessToken accessToken;
    private UserDetails userDetails;


    public UserDetails getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserDetails userDetails) {
        this.userDetails = userDetails;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public long getExpiry() {
        return expiry;
    }

    public void setExpiry(long expiry) {
        this.expiry = expiry;
    }

    public OAuth2AccessToken getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(OAuth2AccessToken accessToken) {
        this.accessToken = accessToken;
    }
}
