package top.lingkang.finaloauth2.entity;

import java.io.Serializable;

/**
 * @author lingkang
 * Created by 2022/3/24
 */
public class OAuth2AccessToken implements Serializable {
    private String token;
    private String refreshToken;
    private long expiry;
    private String scope;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public long getExpiry() {
        return expiry;
    }

    public void setExpiry(long expiry) {
        this.expiry = expiry;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }
}
