package top.lingkang.finaloauth2.entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Set;

/**
 * @author lingkang
 * Created by 2022/3/23
 */
public class AccessToken implements Serializable {
    private int code;
    private String access_token;
    private String refresh_token;
    private int expires_in;
    private String client_id;
    private String scope;
    private String username;
    private Set<String> role;
    private HashMap<String, Object> data;

    @Override
    public String toString() {
        return "AccessToken{" +
                "code=" + code +
                ", access_token='" + access_token + '\'' +
                ", refresh_token='" + refresh_token + '\'' +
                ", expires_in=" + expires_in +
                ", client_id='" + client_id + '\'' +
                ", scope='" + scope + '\'' +
                ", username='" + username + '\'' +
                ", role=" + role +
                ", data=" + data +
                '}';
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public int getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(int expires_in) {
        this.expires_in = expires_in;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Set<String> getRole() {
        return role;
    }

    public void setRole(Set<String> role) {
        this.role = role;
    }

    public HashMap<String, Object> getData() {
        return data;
    }

    public void setData(HashMap<String, Object> data) {
        this.data = data;
    }
}
