package top.lingkang.finaloauth2.constants;


/**
 * @author lingkang
 * Created by 2022/3/16
 */
public class ServerConstants {
    public static final String AUTH_CODE_PREFIX = "auth-code-";
}
