package top.lingkang.finaloauth2.constants;


/**
 * @author lingkang
 * Created by 2022/3/16
 */
public class ClientConstants {

    public static final String UNAUTHORIZED_MSG = "未授权的资源！unauthorized resources!";

    public static final String INVALID_TOKEN="无效的令牌， invalid token";
}
