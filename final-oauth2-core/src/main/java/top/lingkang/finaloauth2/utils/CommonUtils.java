package top.lingkang.finaloauth2.utils;

import top.lingkang.finaloauth2.client.error.Oauth2PermissionException;
import top.lingkang.finaloauth2.constants.ClientConstants;

import java.util.Set;

/**
 * @author lingkang
 * Created by 2022/3/23
 */
public class CommonUtils {
    private static final AntPathMatcher matcher = new AntPathMatcher();

    public static boolean matcher(String pattern, String path) {
        return matcher.match(pattern, path);
    }


    public static void checkRole(String[] roles, Set<String> hasRole) {
        if (roles == null)
            return;
        if (hasRole == null)
            throw new Oauth2PermissionException(ClientConstants.UNAUTHORIZED_MSG);
        for (String r : roles) {
            if(hasRole.contains(r)){
                return;
            }
        }
        throw new Oauth2PermissionException(ClientConstants.UNAUTHORIZED_MSG);
    }

    public static void checkAndRole(String[] roles, Set<String> hasRole) {
        if (roles == null)
            return;
        if (hasRole == null)
            throw new Oauth2PermissionException(ClientConstants.UNAUTHORIZED_MSG);
        for (String r : roles) {
            if (!hasRole.contains(r)){
                throw new Oauth2PermissionException(ClientConstants.UNAUTHORIZED_MSG);
            }
        }
    }
}
