package top.lingkang.finaloauth2.client.error;

/**
 * @author lingkang
 * Created by 2022/3/23
 */
public class TokenInvalidException extends RuntimeException{
    public TokenInvalidException(String message) {
        super(message);
    }
}
