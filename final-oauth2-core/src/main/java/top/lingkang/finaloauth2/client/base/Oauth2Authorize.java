package top.lingkang.finaloauth2.client.base;

import top.lingkang.finaloauth2.utils.CommonUtils;

import java.util.Set;

/**
 * @author lingkang
 * Created by 2022/3/23
 */
public class Oauth2Authorize {
    private String[] role = new String[0], andRole = new String[0];

    public void check(Set<String> hasRole) {
        if (role != null || andRole != null) {
            CommonUtils.checkRole(role, hasRole);
            CommonUtils.checkAndRole(andRole, hasRole);
        }
    }

    public String[] getRole() {
        return role;
    }

    public void setRole(String[] role) {
        this.role = role;
    }

    public String[] getAndRole() {
        return andRole;
    }

    public void setAndRole(String[] andRole) {
        this.andRole = andRole;
    }
}
