package top.lingkang.finaloauth2.client.base;

import top.lingkang.finaloauth2.client.error.BaseClientException;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * @author lingkang
 * Created by 2022/3/23
 */
public class CheckAuthorize {
    // 权限校验
    private HashMap<String, Oauth2Authorize> authorize = new HashMap<>();

    private String tempMatchers;
    private Oauth2Authorize tempFinalAuth;

    public CheckAuthorize pathMatchers(String matchers) {
        tempMatchers = matchers;
        if (authorize.get(tempMatchers) == null) {
            tempFinalAuth = new Oauth2Authorize();
        } else {
            tempFinalAuth = authorize.get(tempMatchers);
        }
        return this;
    }

    public CheckAuthorize hasAnyRole(String... anyRole) {
        if (tempFinalAuth == null) {
            throw new BaseClientException("请先设置匹配路径：pathMatchers");
        }

        Set<String> newRole = new HashSet<>(Arrays.asList(tempFinalAuth.getRole()));
        newRole.addAll(Arrays.asList(anyRole));
        tempFinalAuth.setRole(newRole.toArray(new String[newRole.size()]));
        authorize.put(tempMatchers, tempFinalAuth);
        return this;
    }

    public CheckAuthorize hasAllRole(String... allRole) {
        if (tempFinalAuth == null) {
            throw new BaseClientException("请先设置匹配路径：antMatchers");
        }
        Set<String> newRole = new HashSet<>(Arrays.asList(tempFinalAuth.getRole()));
        newRole.addAll(Arrays.asList(allRole));
        tempFinalAuth.setAndRole(newRole.toArray(new String[newRole.size()]));
        authorize.put(tempMatchers, tempFinalAuth);
        return this;
    }

    public HashMap<String, Oauth2Authorize> getAuthorize() {
        return authorize;
    }

}
