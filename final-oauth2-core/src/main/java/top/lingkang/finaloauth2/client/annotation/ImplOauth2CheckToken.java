package top.lingkang.finaloauth2.client.annotation;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import top.lingkang.finaloauth2.client.http.ResourceServerHolder;

/**
 * @author lingkang
 * Created by 2022/4/3
 */
@Aspect
public class ImplOauth2CheckToken {

    @Autowired(required = false)
    private ResourceServerHolder resourceServerHolder;

    @Around("@within(top.lingkang.finaloauth2.client.annotation.Oauth2CheckToken) || @annotation(top.lingkang.finaloauth2.client.annotation.Oauth2CheckToken)")
    public Object impl(ProceedingJoinPoint joinPoint) throws Throwable {
        resourceServerHolder.getRole();
        return joinPoint.proceed();
    }
}
