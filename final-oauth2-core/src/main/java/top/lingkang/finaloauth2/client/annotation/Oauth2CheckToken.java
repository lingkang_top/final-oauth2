package top.lingkang.finaloauth2.client.annotation;

import java.lang.annotation.*;

/**
 * @author lingkang
 * Created by 2022/4/3
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Oauth2CheckToken {
}
