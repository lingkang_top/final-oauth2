package top.lingkang.finaloauth2.client.http;

import com.alibaba.fastjson.JSONObject;
import top.lingkang.finaloauth2.entity.AccessToken;
import top.lingkang.finaloauth2.entity.OAuth2AccessToken;

import java.util.Set;

/**
 * @author lingkang
 * Created by 2022/3/31
 * 资源服务持有，可以执行所有资源服务操作
 */
public interface ResourceServerHolder {

    /**
     * 密码模式登录
     */
    JSONObject login(String username, String password);

    /**
     * 刷新令牌
     */
    JSONObject refreshToken(String refreshToken);

    /**
     * 注销令牌
     * @param token
     * @return
     */
    JSONObject logout(String token);

    /**
     * 获取当前访问的角色
     */
    Set<String> getRole();

    /**
     * 获取当前访问的最新角色：实时请求授权服务器获取
     */
    Set<String> getRole(boolean isNew);

    /**
     * 获取当前授权令牌的完整访问信息
     */
    AccessToken getAccessToken();

    /**
     * 授权码模式：生成一个授权码
     */
    String generateAuthCode(String redirectUrl,String scope,String state);

    /**
     * 使用授权码获取token
     */
    OAuth2AccessToken authCodeGetToken(String code);

}
