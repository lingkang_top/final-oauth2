package top.lingkang.finaloauth2.client.http;

import java.util.HashMap;
import java.util.List;

/**
 * @author lingkang
 * Created by 2022/3/23
 */
public interface ResourceHttpAccess {
    String get(String url, HashMap<String, List<String>> requestHeader);

    String post(String url, HashMap<String, List<String>> requestHeader, HashMap<String, Object> requestParam);
}
