package top.lingkang.finaloauth2.client.error;

/**
 * @author lingkang
 * Created by 2022/3/23
 */
public class Oauth2HttpAccessException extends RuntimeException {
    public Oauth2HttpAccessException(String message) {
        super(message);
    }
}
