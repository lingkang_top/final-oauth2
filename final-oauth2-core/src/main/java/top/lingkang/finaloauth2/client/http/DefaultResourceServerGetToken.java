package top.lingkang.finaloauth2.client.http;

import top.lingkang.finaloauth2.utils.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * @author lingkang
 * Created by 2022/3/23
 */
public class DefaultResourceServerGetToken implements ResourceServerGetToken {
    @Override
    public String getToken(HttpServletRequest request) {
        String token = request.getParameter("access_token");
        // 参数中
        if (!StringUtils.isEmpty(token)){
            return token;
        }

        // 请求头
        token=request.getHeader("token");
        if (!StringUtils.isEmpty(token)){
            return token;
        }
        return null;
    }
}
