package top.lingkang.finaloauth2.client.http;

import javax.servlet.http.HttpServletRequest;

/**
 * @author lingkang
 * Created by 2022/3/23
 */
public interface ResourceServerGetToken {
    String getToken(HttpServletRequest request);
}
