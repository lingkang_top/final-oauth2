package top.lingkang.finaloauth2.client.error;

/**
 * @author lingkang
 * Created by 2022/3/23
 */
public class BaseClientException extends RuntimeException{
    public BaseClientException(String message) {
        super(message);
    }
}
