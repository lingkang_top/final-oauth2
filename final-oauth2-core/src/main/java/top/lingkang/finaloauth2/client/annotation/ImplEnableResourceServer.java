package top.lingkang.finaloauth2.client.annotation;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import top.lingkang.finaloauth2.client.config.ResourceServerConfiguration;

/**
 * @author lingkang
 * Created by 2022/3/27
 */
public class ImplEnableResourceServer {
    @Bean
    @ConditionalOnMissingBean
    public ResourceServerConfiguration resourceServerConfiguration(){
        return new ResourceServerConfiguration();
    }

}
