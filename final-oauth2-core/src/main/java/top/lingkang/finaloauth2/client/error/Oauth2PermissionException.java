package top.lingkang.finaloauth2.client.error;

/**
 * @author lingkang
 * Created by 2022/3/23
 */
public class Oauth2PermissionException extends RuntimeException{
    public Oauth2PermissionException(String message) {
        super(message);
    }
}
