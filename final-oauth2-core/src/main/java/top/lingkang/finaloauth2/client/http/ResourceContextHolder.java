package top.lingkang.finaloauth2.client.http;

import top.lingkang.finaloauth2.entity.AccessToken;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;

/**
 * @author lingkang
 * Created by 2022/3/23
 * 访问上下文持有者
 */
public abstract class ResourceContextHolder {
    private static final ThreadLocal<HttpServletRequest> request = new ThreadLocal<>();
    private static final ThreadLocal<AccessToken> accessToken = new ThreadLocal<>();
    private static final ThreadLocal<Set<String>> hasRole = new ThreadLocal<>();

    public static HttpServletRequest getRequest() {
        return request.get();
    }

    public static void setRequest(HttpServletRequest req) {
        request.set(req);
    }

    public static void removeAll() {
        request.remove();
        accessToken.remove();
        hasRole.remove();
    }

    public static AccessToken getAccessToken() {
        return accessToken.get();
    }

    public static void setAccessToken(AccessToken token) {
        accessToken.set(token);
    }

    public static Set<String> getRole() {
        return hasRole.get();
    }

    public static void setRole(Set<String> role) {
        hasRole.set(role);
    }

}
