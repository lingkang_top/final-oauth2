package top.lingkang.finaloauth2.client.config;

import top.lingkang.finaloauth2.client.base.CheckAuthorize;
import top.lingkang.finaloauth2.client.base.DefaultResourceExceptionHandler;
import top.lingkang.finaloauth2.client.base.ResourceExceptionHandler;
import top.lingkang.finaloauth2.client.http.DefaultResourceHttpAccess;
import top.lingkang.finaloauth2.client.http.DefaultResourceServerGetToken;
import top.lingkang.finaloauth2.client.http.ResourceHttpAccess;
import top.lingkang.finaloauth2.client.http.ResourceServerGetToken;

import java.util.Arrays;
import java.util.HashSet;

/**
 * @author lingkang
 * Created by 2022/3/23
 */
public class ResourceServerProperties {
    // 授权服务器地址
    private String serverUrl;

    // 客户端配置
    private String clientId;
    private String clientSecret;
    private String scope;


    // 请求服务http接口，可自行实现
    private ResourceHttpAccess resourceHttpAccess = new DefaultResourceHttpAccess();

    // 排除鉴权路径
    private HashSet<String> excludePath = new HashSet<>();

    // 权限校验
    private CheckAuthorize checkAuthorize = new CheckAuthorize();

    // 令牌获取接口
    private ResourceServerGetToken resourceServerGetToken = new DefaultResourceServerGetToken();

    // 自定义异常处理
    private ResourceExceptionHandler resourceExceptionHandler = new DefaultResourceExceptionHandler();


    public String getScope() {
        return scope;
    }

    public ResourceServerProperties setScope(String scope) {
        this.scope = scope;
        return this;
    }

    public String getClientId() {
        return clientId;
    }

    public ResourceServerProperties setClientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public ResourceServerProperties setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
        return this;
    }

    public ResourceHttpAccess getResourceHttpAccess() {
        return resourceHttpAccess;
    }

    public ResourceServerProperties setResourceHttpAccess(ResourceHttpAccess resourceHttpAccess) {
        this.resourceHttpAccess = resourceHttpAccess;
        return this;
    }

    public ResourceServerProperties setExcludePath(HashSet<String> excludePath) {
        this.excludePath = excludePath;
        return this;
    }

    public ResourceServerGetToken getResourceServerGetToken() {
        return resourceServerGetToken;
    }

    public ResourceServerProperties setResourceServerGetToken(ResourceServerGetToken resourceServerGetToken) {
        this.resourceServerGetToken = resourceServerGetToken;
        return this;
    }

    public ResourceExceptionHandler getResourceExceptionHandler() {
        return resourceExceptionHandler;
    }

    public void setResourceExceptionHandler(ResourceExceptionHandler resourceExceptionHandler) {
        this.resourceExceptionHandler = resourceExceptionHandler;
    }

    public ResourceServerGetToken getOauth2GetToken() {
        return resourceServerGetToken;
    }

    public ResourceServerProperties setOauth2GetToken(ResourceServerGetToken resourceServerGetToken) {
        this.resourceServerGetToken = resourceServerGetToken;
        return this;
    }

    public ResourceHttpAccess getOauth2HttpAccess() {
        return resourceHttpAccess;
    }

    public ResourceServerProperties setOauth2HttpAccess(ResourceHttpAccess resourceHttpAccess) {
        this.resourceHttpAccess = resourceHttpAccess;
        return this;
    }

    public String getServerUrl() {
        return serverUrl;
    }

    public ResourceServerProperties setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
        return this;
    }

    public HashSet<String> getExcludePath() {
        return excludePath;
    }

    public ResourceServerProperties setExcludePath(String... excludePath) {
        this.excludePath.addAll(new HashSet<>(Arrays.asList(excludePath)));
        return this;
    }

    public CheckAuthorize getCheckAuthorize() {
        return checkAuthorize;
    }

    public ResourceServerProperties setCheckAuthorize(CheckAuthorize checkAuthorize) {
        this.checkAuthorize = checkAuthorize;
        return this;
    }
}
