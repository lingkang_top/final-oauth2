package top.lingkang.finaloauth2.client.annotation;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author lingkang
 * Created by 2022/3/23
 * 开启oauth2客户端
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Import({ImplOauth2CheckToken.class,ImplOauth2CheckRole.class})
@Documented
public @interface EnableOauth2Annotation {
}
