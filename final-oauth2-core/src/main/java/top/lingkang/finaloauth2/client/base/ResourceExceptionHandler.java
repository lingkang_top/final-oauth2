package top.lingkang.finaloauth2.client.base;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author lingkang
 * Created by 2022/3/23
 */
public interface ResourceExceptionHandler {
    void permissionException(Exception e, HttpServletRequest request, HttpServletResponse response);

    void tokenInvalidException(Exception e, HttpServletRequest request, HttpServletResponse response);

    void exception(Exception e, HttpServletRequest request, HttpServletResponse response);
}
