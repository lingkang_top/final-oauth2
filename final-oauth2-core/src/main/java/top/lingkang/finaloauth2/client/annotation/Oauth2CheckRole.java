package top.lingkang.finaloauth2.client.annotation;

import java.lang.annotation.*;

/**
 * @author lingkang
 * Created by 2022/4/3
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Oauth2CheckRole {
    /**
     * 存在其中任意角色即可通过
     * @return
     */
    String[] anyRole() default {};

    /**
     * 存在其中所有角色即可通过
     * @return
     */
    String[] allRole() default {};
}
