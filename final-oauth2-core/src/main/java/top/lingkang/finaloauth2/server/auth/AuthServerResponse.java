package top.lingkang.finaloauth2.server.auth;

import com.alibaba.fastjson.JSONObject;
import top.lingkang.finaloauth2.entity.Oauth2Code;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author lingkang
 * Created by 2022/3/25
 */
public interface AuthServerResponse {
    Object check_token(HttpServletRequest request);

    Object info(HttpServletRequest request);

    Object authority(HttpServletRequest request);

    Object passwordAuthSuccess(String token, String refreshToken, long tokenExpires, long refreshExpires, String scope,
                               HttpServletRequest request, HttpServletResponse response);

    Object logout(JSONObject result);
}
