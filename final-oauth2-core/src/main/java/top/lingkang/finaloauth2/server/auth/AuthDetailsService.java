package top.lingkang.finaloauth2.server.auth;


import top.lingkang.finaloauth2.entity.Oauth2Code;
import top.lingkang.finaloauth2.entity.UserDetails;
import top.lingkang.finaloauth2.server.client.ClientDetails;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;

/**
 * @author lingkang
 * Created by 2022/3/24
 */
public interface AuthDetailsService {
    /**
     * 密码模式获取用户详情
     * @param username
     * @param password
     * @param request
     * @return
     */
    UserDetails loginUser(String username, String password, HttpServletRequest request);

    /**
     * 授权码模式获取授权的权限，对将要颁发的token进行令牌角色赋值
     * @param oauth2Code
     * @param clientDetails
     * @return
     */
    Set<String> codeAuthorizationRole(Oauth2Code oauth2Code, ClientDetails clientDetails, HttpServletRequest request);
}
