package top.lingkang.finaloauth2.server.error;

/**
 * @author lingkang
 * Created by 2022/3/24
 */
public class AuthServerException extends ServerException{
    public AuthServerException(String message) {
        super(message);
    }
}
