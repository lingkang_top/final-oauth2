package top.lingkang.finaloauth2.server.annotation;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import top.lingkang.finaloauth2.server.config.AuthorizationServerConfiguration;

/**
 * @author lingkang
 * Created by 2022/3/27
 */
public class EnableAuthorizationServerImpl {
    @Bean
    @ConditionalOnMissingBean
    public AuthorizationServerConfiguration authorizationServerConfiguration() {
        return new AuthorizationServerConfiguration();
    }
}
