package top.lingkang.finaloauth2.server.store;

import top.lingkang.finaloauth2.entity.*;

/**
 * @author lingkang
 * Created by 2022/3/24
 * 处理获取时，已经失效应该在实现时处理，并返回空
 * 令牌的存储，，获取令牌或其他值时，必须要先验证是否到期了！即，所有获取值都会验证时效
 */
public interface TokenStore {

    OAuth2AccessToken getAccessToken(String token);

    UserTokenInfo getUserTokenInfo(String username, String clientId);

    /**
     * 移除token，此操作将token有关的全删除，不包括刷新token！
     */
    OAuth2AccessToken removeAccessToken(String token);

    void storeAccessToken(OAuth2AccessToken accessToken, UserDetails userDetails);

    UserDetails getUserDetails(String token);

    OAuth2RefreshToken getRefreshToken(String refreshToken);

    /**
     * 移除刷新token时，会把对应的token移除掉
     */
    OAuth2RefreshToken removeRefreshToken(String refreshToken);

    void storeRefreshToken(OAuth2RefreshToken refreshToken);

    void setCode(Oauth2Code oauth2Code);

    Oauth2Code getOauth2Code(String code);

}
