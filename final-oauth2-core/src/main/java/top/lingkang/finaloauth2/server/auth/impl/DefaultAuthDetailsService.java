package top.lingkang.finaloauth2.server.auth.impl;

import top.lingkang.finaloauth2.entity.Oauth2Code;
import top.lingkang.finaloauth2.server.auth.AuthDetailsService;
import top.lingkang.finaloauth2.entity.UserDetails;
import top.lingkang.finaloauth2.server.client.ClientDetails;
import top.lingkang.finaloauth2.server.error.AuthServerUserException;

import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;
import java.util.Set;

/**
 * @author lingkang
 * Created by 2022/3/24
 */
public class DefaultAuthDetailsService implements AuthDetailsService {
    @Override
    public UserDetails loginUser(String username, String password, HttpServletRequest request) {
        if (!"final".equals(username)) {
            throw new AuthServerUserException("用户名不存在！");
        } else if (!"final".equals(password)) {
            throw new AuthServerUserException("密码错误！");
        }
        Set<String> auth = new HashSet<>();
//        auth.add("user");
//        auth.add("admin");
//        auth.add("tenant");
        UserDetails details = new UserDetails("final", auth);
        return details;
    }

    @Override
    public Set<String> codeAuthorizationRole(Oauth2Code oauth2Code, ClientDetails clientDetails, HttpServletRequest request) {
        HashSet<String> role = new HashSet<>();
        // role.add("tenant");
        return role;
    }
}
