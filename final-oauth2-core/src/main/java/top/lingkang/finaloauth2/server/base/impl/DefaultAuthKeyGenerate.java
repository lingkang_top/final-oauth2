package top.lingkang.finaloauth2.server.base.impl;

import top.lingkang.finaloauth2.server.base.AuthKeyGenerate;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

/**
 * @author lingkang
 * Created by 2022/3/23
 */
public class DefaultAuthKeyGenerate implements AuthKeyGenerate {
    @Override
    public String token(HttpServletRequest request) {
        return UUID.randomUUID().toString();
    }

    @Override
    public String refreshToken(HttpServletRequest request) {
        return UUID.randomUUID().toString();
    }

    @Override
    public String code(HttpServletRequest request) {
        return UUID.randomUUID().toString();
    }
}
