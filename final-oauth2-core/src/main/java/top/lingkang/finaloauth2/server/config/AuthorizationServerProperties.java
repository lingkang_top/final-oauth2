package top.lingkang.finaloauth2.server.config;

import top.lingkang.finaloauth2.server.base.AuthKeyGenerate;
import top.lingkang.finaloauth2.server.error.AuthServerExceptionHandler;
import top.lingkang.finaloauth2.server.base.impl.DefaultAuthKeyGenerate;
import top.lingkang.finaloauth2.server.store.TokenStore;
import top.lingkang.finaloauth2.server.store.impl.InMemoryTokenStore;

/**
 * @author lingkang
 * Created by 2022/3/24
 */
public class AuthorizationServerProperties {
    // 令牌到期时间 30分钟
    private long tokenMaxExpires = 1800000L;
    // 刷新令牌到期时间 15天
    private long refreshTokenMaxExpires = 1296000000L;
    // 授权码失效时间 默认5分钟后失效
    private long authCodeMaxExpires=300000L;


    private AuthKeyGenerate authKeyGenerate = new DefaultAuthKeyGenerate();

    private TokenStore tokenStore = new InMemoryTokenStore();

    private AuthServerExceptionHandler authServerExceptionHandler;



    public long getAuthCodeMaxExpires() {
        return authCodeMaxExpires;
    }

    public void setAuthCodeMaxExpires(long authCodeMaxExpires) {
        this.authCodeMaxExpires = authCodeMaxExpires;
    }

    public AuthServerExceptionHandler getAuthServerExceptionHandler() {
        return authServerExceptionHandler;
    }

    public AuthorizationServerProperties setAuthServerExceptionHandler(AuthServerExceptionHandler authServerExceptionHandler) {
        this.authServerExceptionHandler = authServerExceptionHandler;
        return this;
    }

    public long getTokenMaxExpires() {
        return tokenMaxExpires;
    }

    public TokenStore getTokenStore() {
        return tokenStore;
    }

    public AuthorizationServerProperties setTokenStore(TokenStore tokenStore) {
        this.tokenStore = tokenStore;
        return this;
    }

    public AuthorizationServerProperties setTokenMaxExpires(long tokenMaxExpires) {
        this.tokenMaxExpires = tokenMaxExpires;
        return this;
    }

    public long getRefreshTokenMaxExpires() {
        return refreshTokenMaxExpires;
    }

    public AuthorizationServerProperties setRefreshTokenMaxExpires(long refreshTokenMaxExpires) {
        this.refreshTokenMaxExpires = refreshTokenMaxExpires;
        return this;
    }

    public AuthKeyGenerate getAuthKeyGenerate() {
        return authKeyGenerate;
    }

    public AuthorizationServerProperties setAuthKeyGenerate(AuthKeyGenerate authKeyGenerate) {
        this.authKeyGenerate = authKeyGenerate;
        return this;
    }
}
