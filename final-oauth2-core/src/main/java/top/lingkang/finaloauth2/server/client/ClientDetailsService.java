package top.lingkang.finaloauth2.server.client;

/**
 * @author lingkang
 * Created by 2022/3/24
 */
public interface ClientDetailsService {
    ClientDetails loadClientByClientId(String clientId);
}
