package top.lingkang.finaloauth2.server.base;

import javax.servlet.http.HttpServletRequest;

/**
 * @author lingkang
 * Created by 2022/3/18
 * 可自定义token、刷新token的生成
 */
public interface AuthKeyGenerate {
    /**
     * 生成令牌
     * @param request
     * @return
     */
    String token(HttpServletRequest request);

    /**
     * 生成刷新令牌
     * @param request
     * @return
     */
    String refreshToken(HttpServletRequest request);

    /**
     * 生成授权码
     * @param request
     * @return
     */
    String code(HttpServletRequest request);
}
