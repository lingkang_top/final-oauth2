package top.lingkang.finaloauth2.server.error;

/**
 * @author lingkang
 * Created by 2022/3/16
 */
public class ServerException extends RuntimeException{
    public ServerException(String message) {
        super(message);
    }

    public ServerException(String message, Throwable cause) {
        super(message, cause);
    }
}
