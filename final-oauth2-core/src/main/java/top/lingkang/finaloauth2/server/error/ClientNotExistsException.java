package top.lingkang.finaloauth2.server.error;

/**
 * @author lingkang
 * Created by 2022/3/24
 */
public class ClientNotExistsException extends ServerException{
    public ClientNotExistsException(String message) {
        super(message);
    }
}
