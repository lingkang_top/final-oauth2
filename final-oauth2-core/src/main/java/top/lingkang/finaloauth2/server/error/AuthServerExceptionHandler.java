package top.lingkang.finaloauth2.server.error;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author lingkang
 * Created by 2022/3/24
 * 可实现注入该接口，实现服务端异常定制处理
 */
public interface AuthServerExceptionHandler {
    void exception(Exception e, HttpServletRequest request, HttpServletResponse response);
}
