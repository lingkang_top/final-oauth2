package top.lingkang.finaloauth2.server.auth;

import javax.servlet.http.HttpServletRequest;

/**
 * @author lingkang
 * Created by 2022/3/23
 */
public interface AuthServerGetToken {
    String getToken(HttpServletRequest request);

    String getRefreshToken(HttpServletRequest request);
}
