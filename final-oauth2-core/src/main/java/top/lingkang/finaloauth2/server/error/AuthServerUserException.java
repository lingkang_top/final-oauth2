package top.lingkang.finaloauth2.server.error;

/**
 * @author lingkang
 * Created by 2022/3/25
 */
public class AuthServerUserException extends ServerException{
    public AuthServerUserException(String message) {
        super(message);
    }
}
