package top.lingkang.finaloauth2.server.error;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author lingkang
 * Created by 2022/3/24
 */
public class DefaultAuthServerExceptionHandler implements AuthServerExceptionHandler {
    private static final Log log = LogFactory.getLog(DefaultAuthServerExceptionHandler.class);

    @Override
    public void exception(Exception e, HttpServletRequest request, HttpServletResponse response) {
        log.error(e);
        JSONObject res = new JSONObject();
        res.put("code", 1);
        res.put("timestamp", System.currentTimeMillis());
        res.put("msg", e.getMessage());
        try {
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(res.toJSONString());
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }
}
