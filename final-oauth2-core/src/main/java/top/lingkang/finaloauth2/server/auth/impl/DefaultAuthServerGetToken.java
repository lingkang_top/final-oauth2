package top.lingkang.finaloauth2.server.auth.impl;

import top.lingkang.finaloauth2.server.auth.AuthServerGetToken;
import top.lingkang.finaloauth2.utils.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * @author lingkang
 * Created by 2022/3/24
 */
public class DefaultAuthServerGetToken implements AuthServerGetToken {
    @Override
    public String getToken(HttpServletRequest request) {
        String token = request.getParameter("access_token");
        // 参数中
        if (!StringUtils.isEmpty(token)){
            return token;
        }

        // 请求头
        token=request.getHeader("token");
        if (!StringUtils.isEmpty(token)){
            return token;
        }
        return null;
    }

    @Override
    public String getRefreshToken(HttpServletRequest request) {
        String token = request.getParameter("refresh_token");
        // 参数中
        if (!StringUtils.isEmpty(token)){
            return token;
        }

        // 请求头
        token=request.getHeader("refresh_token");
        if (!StringUtils.isEmpty(token)){
            return token;
        }
        return null;
    }
}
