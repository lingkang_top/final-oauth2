package top.lingkang.finaloauth2.server.client;

import java.util.Set;

/**
 * @author lingkang
 * Created by 2022/3/24
 */
public class ClientDetails {
    private String clientId;
    private String clientSecret;
    private Set<String> grantType;
    private String scope;

    // 授权码相关
    private Set<String> ip;// 允许重定向授权ip


    public Set<String> getIp() {
        return ip;
    }

    public void setIp(Set<String> ip) {
        this.ip = ip;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public Set<String> getGrantType() {
        return grantType;
    }

    public void setGrantType(Set<String> grantType) {
        this.grantType = grantType;
    }

}
