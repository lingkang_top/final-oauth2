package top.lingkang.finaloauth2.server.auth;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author lingkang
 * Created by 2022/3/24
 */
public interface AuthorizationMode {

    Object token(HttpServletRequest request, HttpServletResponse response);

    Object code(HttpServletRequest request, HttpServletResponse response);
}
