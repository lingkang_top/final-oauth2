package top.lingkang.finaloauth2.server.auth.impl;

import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import top.lingkang.finaloauth2.entity.OAuth2AccessToken;
import top.lingkang.finaloauth2.entity.UserDetails;
import top.lingkang.finaloauth2.server.auth.AuthServerGetToken;
import top.lingkang.finaloauth2.server.auth.AuthServerResponse;
import top.lingkang.finaloauth2.server.store.TokenStore;
import top.lingkang.finaloauth2.utils.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author lingkang
 * Created by 2022/3/25
 */
public class DefaultAuthServerResponse implements AuthServerResponse {
    @Autowired
    private TokenStore tokenStore;
    @Autowired
    private AuthServerGetToken authServerGetToken;

    @Override
    public Object check_token(HttpServletRequest request) {
        String token = authServerGetToken.getToken(request);
        if (StringUtils.isEmpty(token)) {
            return "{\"code\":1,\"msg\":\"无效的token invalid token\",\"timestamp\":" + System.currentTimeMillis() + "}";
        }
        OAuth2AccessToken accessToken = tokenStore.getAccessToken(token);
        if (accessToken == null) {
            return "{\"code\":1,\"msg\":\"无效的token invalid token\",\"timestamp\":" + System.currentTimeMillis() + "}";
        }
        JSONObject res = new JSONObject();
        res.put("code", 0);
        res.put("access_token", token);
        res.put("refresh_token", accessToken.getRefreshToken());
        res.put("expires_in", (accessToken.getExpiry() - System.currentTimeMillis()) / 1000);
        res.put("scope", accessToken.getScope());
        return res.toJSONString();
    }

    @Override
    public Object info(HttpServletRequest request) {
        String token = authServerGetToken.getToken(request);
        if (StringUtils.isEmpty(token)) {
            return "{\"code\":1,\"msg\":\"无效的token invalid token\",\"timestamp\":" + System.currentTimeMillis() + "}";
        }
        OAuth2AccessToken accessToken = tokenStore.getAccessToken(token);
        if (accessToken == null) {
            return "{\"code\":1,\"msg\":\"无效的token invalid token\",\"timestamp\":" + System.currentTimeMillis() + "}";
        }
        UserDetails userDetails = tokenStore.getUserDetails(token);
        JSONObject res = new JSONObject();
        res.put("code", 0);
        res.put("access_token", token);
        res.put("refresh_token", accessToken.getRefreshToken());
        res.put("expires_in", (accessToken.getExpiry() - System.currentTimeMillis()) / 1000);
        res.put("scope", accessToken.getScope());
        res.put("role", userDetails.getRole());
        res.put("username", userDetails.getUsername());
        res.put("client_id", userDetails.getClientId());
        res.put("data", userDetails.getData());
        return res;
    }

    @Override
    public Object authority(HttpServletRequest request) {
        String token = authServerGetToken.getToken(request);
        if (StringUtils.isEmpty(token)) {
            return null;
        }
        UserDetails userDetails = tokenStore.getUserDetails(token);
        if (userDetails == null) {
            return null;
        }
        return userDetails.getRole();
    }

    @Override
    public Object passwordAuthSuccess(String token, String refreshToken, long tokenExpires, long refreshExpires, String scope, HttpServletRequest request, HttpServletResponse response) {
        JSONObject res = new JSONObject();
        res.put("code", 0);
        res.put("access_token", token);
        res.put("refresh_token", refreshToken);
        res.put("token_expires", tokenExpires);
        res.put("refresh_expires", refreshExpires);
        res.put("scope", scope);
        return res.toJSONString();
    }

    @Override
    public Object logout(JSONObject result) {
        return result;
    }
}
