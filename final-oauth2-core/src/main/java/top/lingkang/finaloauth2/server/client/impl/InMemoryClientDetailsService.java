package top.lingkang.finaloauth2.server.client.impl;

import top.lingkang.finaloauth2.server.client.ClientDetails;
import top.lingkang.finaloauth2.server.client.ClientDetailsService;

import java.util.HashMap;

/**
 * @author lingkang
 * Created by 2022/3/24
 */
public class InMemoryClientDetailsService implements ClientDetailsService {
    private HashMap<String, ClientDetails> detailsHashMap;

    public InMemoryClientDetailsService(HashMap<String, ClientDetails> detailsHashMap) {
        this.detailsHashMap = detailsHashMap;
    }

    @Override
    public ClientDetails loadClientByClientId(String clientId) {
        return detailsHashMap.get(clientId);
    }
}
